Sul file Functions.php troverete la creazione del custom Post type "Products" e della custom taxonomy "tipo",
così da dividere i prodotti in varie tipologia

Ognuna di queste tipologie ha la propria pagina a cui si può accedere dal menù o dalla gallery in home page
cliccando sulle schede testuali.

In ogni post di "Products" si trovano dei custom fields creati con ACF con cui si può scegliere
l'immagine, il nome del prodotto e il prezzo.

Ogni pagina ha il suo template.

Ho aggiunto WPML e tradotto parte delle stringhe del tema custom.

