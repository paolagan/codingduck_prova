<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'codingduck' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', 'root' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '1>h!>ew)}X(>+d2wD?nqA+<WONQp.bW2zEs!Q?X*NapYY:EK.M%-bj4i Zl_6Kd|' );
define( 'SECURE_AUTH_KEY',  '575@s7!MI**4jU$o4HO%^}o5bjMA&0(X{Rze4KYx5!DatzQ(OoKJsZ%RV%#|.#t,' );
define( 'LOGGED_IN_KEY',    'fMgbafA>dz8bUUO}<Sb:Vkefv#RI5XX6CV!M-0a8)2$gSpUtn,k`9A]IG}CF<n?>' );
define( 'NONCE_KEY',        'YqSt4T/CL$>&QTLM@!*6`5#U23)xa`^d$VV{auD8,__g^sR-d^]T88e)`YEXf,`[' );
define( 'AUTH_SALT',        'C.l~LQtF8hS7Jzlmb?z>`$,D-<.3oq{m:_R@j8*q|K+d!o<j?d|wGV8Np^ vlIP}' );
define( 'SECURE_AUTH_SALT', '`D`;OZxGdW.eqvpG~wc-_C8o+`Z28b!n9xX[$(hmA0o[@7ZX<L-Vh!B/at^YkQ>q' );
define( 'LOGGED_IN_SALT',   ' >#.!u1;r1Eg(=@w:2Lo(mDmE(9)?0_<!9qT* (:SIGYrsM#?Ud`@DGV1<_%om!<' );
define( 'NONCE_SALT',       '0%JS791%>)sy[Htt?lJw?O-U+%?f_.vgWg:Y=$xErG<,wN(V6jd/KkxL{xII.1;#' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
