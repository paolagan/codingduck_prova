<?php

/**
 * Add custom taxonomies
 *
 * Additional custom taxonomies can be defined here
 * https://codex.wordpress.org/Function_Reference/register_taxonomy
 */


function add_tipo_taxonomies() {

  // Add new "Prodotti" taxonomy to Posts
  register_taxonomy('tipo', 'products', array(
    // Hierarchical taxonomy (like categories)
    'hierarchical' => true,
    'show_in_rest' => true,
    'public'=> true,
    'show_ui' => true,

    // This array of options controls the labels displayed in the WordPress Admin UI
    'labels' => array(
      'name' => _x( 'Tipi', 'taxonomy general name' ),
      'singular_name' => _x( 'Tipo', 'taxonomy singular name' ),
      'search_items' =>  __( 'Search Tipi' ),
      'all_items' => __( 'All Tipi' ),
      'parent_item' => __( 'Parent Tipo' ),
      'parent_item_colon' => __( 'Parent Tipo:' ),
      'edit_item' => __( 'Edit Tipo' ),
      'update_item' => __( 'Update Tipo' ),
      'add_new_item' => __( 'Add New Tipo' ),
      'new_item_name' => __( 'New Tipo Name' ),
      'menu_name' => __( 'Tipi' ),
    ),
    // Control the slugs used for this taxonomy
    'rewrite' => array(
      'slug' => 'tipi', // This controls the base slug that will display before each term
      'query_var'=> true,
      'with_front' => true, // Don't display the category base before "/locations/"
      'hierarchical' => true // This will allow URL's like "/locations/boston/cambridge/"
    ),
  ));
}
add_action( 'init', 'add_tipo_taxonomies', 0);





/* Custom Post Type Start */
function create_posttype() {
  register_post_type(
    'products',
    // CPT Options
    array(
      'labels' => array(
        'name' => __( 'products' ),
        'singular_name' => __( 'Products' )
      ),
      'public' => true,
      'has_archive' => false,
      'rewrite' => array('slug' => 'products'),
    )
  );
}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );
/* Custom Post Type End */


/*Custom Post type start*/
function cw_post_type_products() {
  $supports = array(
    'title', // post title
    'editor', // post content
    'author', // post author
    'thumbnail', // featured images
    'excerpt', // post excerpt
    'custom-fields', // custom fields
    'comments', // post comments
    'revisions', // post revisions
    'post-formats', // post formats
  );
  $labels = array(
    'name' => _x('products', 'plural'),
    'singular_name' => _x('product', 'singular'),
    'menu_name' => _x('Products', 'admin menu'),
    'name_admin_bar' => _x('products', 'admin bar'),
    'add_new' => _x('Add Product', 'add new'),
    'add_new_item' => __('Add New products'),
    'new_item' => __('New products'),
    'edit_item' => __('Edit products'),
    'view_item' => __('View products'),
    'all_items' => __('All products'),
    'search_items' => __('Search products'),
    'not_found' => __('No products found.'),
  );
  $args = array(
    'supports' => $supports,
    'labels' => $labels,
    'public' => true,
    'query_var' => true,
    'rewrite' => array('slug' => 'products'),
    'has_archive' => true,
    'hierarchical' => false,
  );
  register_post_type('products', $args);
}

add_action('init', 'cw_post_type_products');

/*Custom Post type end*/

/* custom menu*/
function register_my_menu() {
register_nav_menu('menu',__( 'menu' ));
}
add_action( 'init', 'register_my_menu' );



?>
