<?php /*Template Name: Taxonomy - Terra*/ ?>

<?php
get_header();
get_template_part('include/navbar');
//print_r(get_the_terms($post, "prodotto")[0]->slug);
$general_url = get_template_directory_uri();
$posts = get_posts(array(
   'post_type' => 'products',
   'posts_per_page'  => '-1',
));
$tax_term = get_the_terms($posts[0] , "tipo")[0]->slug;
//print_r( $tax_term );

?>

<!--Prodotti da forno content -->
<main id="site-content">
  <div  class="latest_news">
     <div class="container">
        <div class="row">
           <div class="col-md-12">
              <div class="titlepage">
                 <h2><?php _e( 'Prodotti da', 'my-plugin-domain' ); ?> <span class="green"><?php _e( 'Forno', 'my-plugin-domain' ); ?></span></h2>
              </div>
           </div>
        </div>
        <div class="row">

          <?php  foreach ($posts as $post) {
            if (get_the_terms($post, "tipo")[0]->slug == "forno") {
          ?>

              <div class="col-md-4 offset-md-2">
                <a href="<?php the_permalink() ?>">
                 <div id="new" class="news_box">
                    <div class="news_img">

                       <figure><img src="<?php the_field('immagine_prodotto'); ?>" alt="#"/></figure>
                    </div>
                    <div class="news_room">
                       <p><?php echo the_terms($post, "tipo"); ?></p>
                       <h3><?php echo the_field('nome_prodotto'); ?></h3>
                       <h5>Prezzo : <?php echo the_field('prezzo');?></h5>
                       <p><?php echo $post->post_content; ?> </p>
                    </div>
                 </div>
               </a>
              </div>
            <?php } ?>

          <?php } ?>

        </div>
     </div>
  </div>
</main>
<!-- end content -->
<?php

get_footer();
