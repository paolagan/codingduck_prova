<?php $general_url = get_site_url(); ?>
<!-- header -->
<header>
   <!-- header inner -->
   <div class="header">
      <div class="container d-flex justify-content-center">
         <div class="row">
            <div class="col-md-12 col-sm-3 col logo_section">
               <div class="full">
                  <div class="center-desk">
                     <div class="logo">
                        <a href="<?php echo $general_url; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logofoog.png" alt="#" /></a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-10 offset-md-1">
               <nav class="navigation navbar navbar-expand-md navbar-dark ">
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                  </button>
                  <div class="collapse navbar-collapse" id="navbarsExample04">
                    <?php wp_nav_menu( array( 'theme_location' => 'menu' ) ); ?>

                    <!--
                    <ul class="navbar-nav mr-auto">
                       <li class="nav-item active">
                          <a class="nav-link" href="<?php //echo $general_url; ?>/home">Home</a>
                       </li>
                       <li class="nav-item">
                          <a class="nav-link" href="<?php// echo $general_url; ?>/prodotti">Prodotti</a>
                       </li>
                       <li class="nav-item">
                          <a class="nav-link" href="<?php //echo $general_url; ?>/tipi/forno">Forno</a>
                       </li>
                       <li class="nav-item">
                          <a class="nav-link" href="<?php //echo $general_url; ?>/tipi/dolci">Dolci</a>
                       </li>
                       <li class="nav-item">
                          <a class="nav-link" href="<?php// echo $general_url; ?>/tipi/terra">Terra</a>
                       </li>
                    </ul>
                    -->

                  </div>
               </nav>
            </div>
         </div>
      </div>
   </div>
</header>
<!-- end header inner -->
<!-- end header -->

<style media="screen">

</style>
