<?php /*Template Name: Prova*/ ?>
<?php
get_header();
get_template_part('include/navbar');

$general_url = get_template_directory_uri();
$category_name = wp_get_post_categories(1);
//print_r($category_name);
$posts = get_posts(array(
   'post_type' => 'products',
   'posts_per_page'  => '-1',
));
//$image = get_field('immagine_prodotto');
//$taxonomy_names = get_term_by('slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
$tax_term = get_the_terms($posts[0], "tipo");
//print_r( $tax_term );die;
?>

<main id="site-content">
  <div  class="latest_news">
     <div class="container">
        <div class="row">
           <div class="col-md-12">
              <div class="titlepage">
                 <h2>Tutti i nostri <span class="green">prodotti</span></h2>
              </div>
           </div>
        </div>
        <div class="row">

          <?php  foreach ($posts as $post) { ?>

              <div class="col-md-4 offset-md-2">
                <a href="<?php the_permalink() ?>">
                 <div id="new" class="news_box">
                    <div class="news_img">
                       <figure><img src="<?php the_field('immagine_prodotto'); ?>" alt="#"/></figure>
                    </div>
                    <div class="news_room">
                       <p><?php echo the_terms($post, "tipo"); ?></p>
                       <h3><?php echo the_field('nome_prodotto'); ?></h3>
                       <h5>Prezzo : <?php echo the_field('prezzo');?></h5>
                       <p><?php echo $post->post_content; ?> </p>
                    </div>
                 </div>
               </a>
              </div>

          <?php } ?>

        </div>
     </div>
  </div>
</main>
<!-- #site-content -->


<?php
get_footer();
