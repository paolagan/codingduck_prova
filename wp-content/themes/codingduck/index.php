<?php /* Template Name: codingduckProject - HomePage */ ?>
<?php
   $general_url = get_template_directory_uri();
   //print_r($general_url);die;
   get_header();
?>

      <!-- navbar -->
      <?php get_template_part('include/navbar');?>
      <!-- end navbar -->

      <!-- banner -->
      <section class="banner_main">
         <div class="banner">
            <div class="container">
              <div class="row">
                 <div class="col-md-7 offset-md-5">
                    <div class="text-bg">
                       <h1> <?php _e( 'Amanti del cibo,', 'my-plugin-domain' ); ?> <br> <?php _e( 'ecco i nostri prodotti!', 'my-plugin-domain' ); ?></h1>
                       <a href="<?php echo $general_url; ?>/prodotti" class="read_more"><?php _e( 'Scopri', 'my-plugin-domain' ); ?></a>
                    </div>
                 </div>
              </div>
            </div>
         </div>
      </section>
      <!-- end banner -->

      <!-- gallery -->
      <div id="gallery"  class="gallery">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="titlepage">
                     <h2><?php _e( 'I nostri', 'my-plugin-domain' ); ?> <span class="green"><?php _e( 'Prodotti', 'my-plugin-domain' ); ?></span></h2>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-4 col-sm-6">
                 <a href="<?php echo get_site_url(); ?>/tipi/forno">
                   <div class="gallery_text">
                      <div class="galleryh3">
                         <h3><?php _e( 'Prodotti da forno', 'my-plugin-domain' ); ?></h3>
                      </div>
                   </div>
                 </a>
               </div>
               <div class="col-md-4 col-sm-6">
                  <div class="gallery_img">
                     <figure><img src="<?php echo $general_url; ?>/images/gallery1.jpg" alt="#"/></figure>
                  </div>
               </div>
               <div class="col-md-4 col-sm-6">
                  <div class="gallery_img">
                     <figure><img src="<?php echo $general_url; ?>/images/gallery2.jpg" alt="#"/></figure>
                  </div>
               </div>
               <div class="col-md-4 col-sm-6">
                  <div class="gallery_img">
                     <figure><img src="<?php echo $general_url; ?>/images/gallery3.jpg" alt="#"/></figure>
                  </div>
               </div>
               <div class="col-md-4 col-sm-6">
                  <div class="gallery_img">
                     <figure><img src="<?php echo $general_url; ?>/images/gallery4.jpg" alt="#"/></figure>
                  </div>
               </div>
               <div class="col-md-4 col-sm-6">
                 <a href="<?php echo get_site_url(); ?>/tipi/dolci">
                   <div class="gallery_text">
                      <div class="galleryh3">
                         <h3><?php _e( 'Prodotti Dolciari', 'my-plugin-domain' ); ?></h3>
                      </div>
                   </div>
                 </a>
               </div>
               <div class="col-md-4 col-sm-6">
                 <a href="<?php echo get_site_url(); ?>/tipi/terra">
                   <div class="gallery_text">
                      <div class="galleryh3">
                         <h3><?php _e( 'Prodotti della terra', 'my-plugin-domain' ); ?></h3>
                      </div>
                   </div>
                 </a>
               </div>
               <div class="col-md-4 col-sm-6">
                  <div class="gallery_img">
                     <figure><img src="<?php echo $general_url; ?>/images/gallery5.jpg" alt="#"/></figure>
                  </div>
               </div>
               <div class="col-md-4 col-sm-6">
                  <div class="gallery_img">
                     <figure><img src="<?php echo $general_url; ?>/images/gallery6.jpg" alt="#"/></figure>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- end gallery -->

      <!--  contact -->
      <div id="contact" class="contact">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="titlepage">
                     <h2><?php _e( 'Contattaci', 'my-plugin-domain' ); ?> </h2>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-6">
                  <form id="request" class="main_form">
                     <div class="row">
                        <div class="col-md-12 ">
                           <input class="contactus" placeholder="Name" type="type" name="Name">
                        </div>
                        <div class="col-md-12">
                           <input class="contactus" placeholder="Email" type="type" name="Email">
                        </div>
                        <div class="col-md-12">
                           <input class="contactus" placeholder="Phone Number" type="type" name="Phone Number">
                        </div>
                        <div class="col-md-12">
                           <textarea class="textarea" placeholder="Message" type="type" Message="Name">Message</textarea>
                        </div>
                        <div class="col-md-12">
                           <button class="send_btn">Send</button>
                        </div>
                     </div>
                  </form>
               </div>
               <div class="col-md-6">

               </div>
            </div>
         </div>
      </div>
<!-- end contact -->
<?php get_footer() ?>
