<?php /*Template Name: Taxonomy - Forno*/ ?>

<?php
get_header();
get_template_part('include/navbar');
//print_r(get_the_terms($post, "prodotto")[0]->slug);
$general_url = get_template_directory_uri();
$posts = get_posts(array(
   'post_type' => 'products',
   'posts_per_page'  => '-1',
));
$tax_term = get_the_terms($posts[0] , "tipo")[0]->slug;
//print_r( $tax_term );
?>

<!--Prodotti da forno content -->
<main id="site-content">
  <div  class="latest_news">
     <div class="container">
        <div class="row">
           <div class="col-md-12">
              <div class="titlepage">
                 <h2><?php the_title(); ?></span></h2>
              </div>
           </div>
        </div>
        <div class="row">
            <div class="col-md-4 offset-md-2">
               <div id="new" class="news_products">
                  <div class="products_img">
                     <figure><img src="<?php the_field('immagine_prodotto'); ?>" alt="#"/></figure>
                  </div>
               </div>
            </div>

            <div class="col-md-4 offset-md-2">
               <div id="new" class="single_product_box">
                  <div class="single_product">
                     <p class="text-white"><?php echo the_terms($post, "tipo"); ?></p>
                     <h1 class="text-white font-weight-bold"><?php echo the_field('nome_prodotto'); ?></h1>
                     <h3 class="text-white font-weight-bold">Prezzo : <?php echo the_field('prezzo');?></h3>
                     <p class="text-white"><?php echo $post->post_content; ?> </p>
                  </div>
               </div>
            </div>
        </div>
     </div>
  </div>
</main>
<!-- end content -->
<?php

get_footer();
