<?php
 $general_url = get_template_directory_uri();
?>
    <!--  footer -->
    <footer>
       <div class="footer">
          <div class="container d-flex justify-content-center">
             <div class="row">
                <div class=" col-md-12 col-sm-6">
                   <ul class="social_icon">
                      <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                      <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                      <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                      <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                   </ul>
                </div>

             </div>
          </div>
          <div class="copyright">
             <div class="container">
                <div class="row">
                   <div class="col-md-10 offset-md-1">
                      <p>Credits <a href="https://html.design/"> Paola G.</a></p>
                   </div>
                </div>
             </div>
          </div>
       </div>
    </footer>
    <!-- end footer -->
    <!-- Javascript files-->
    <script src="<?php echo $general_url; ?>/js/jquery.min.js"></script>
    <script src="<?php echo $general_url; ?>/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo $general_url; ?>/js/jquery-3.0.0.min.js"></script>
    <!-- sidebar -->
    <script src="<?php echo $general_url; ?>/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="<?php echo $general_url; ?>/js/custom.js"></script>
  </body>
</html>
